function [ fitness ] = esn_fitfun( genCode, obsData_tr, obsData_ts, futureData_tr, futureData_ts, config )
%FITFUN 
%
% INPUT:
% genCode(1): reservoir_size - integer in [100,1000]
% genCode(2): desired_specrad - real in [0.5,0.99]
% genCode(3): noise_level - real in [0.00001, 0.001]
% genCode(4): lambda - real in [2^-10, 2^10]
% genCode(5): input scaling - real in [0.1, 1] 
% genCode(6): feedback scaling - real in [0, 1]
% genCode(7): teacher scaling - real in [0.1, 1]


% config
config.reservoir_size = 10*genCode(1);       % Size of the reservoir
config.desired_specrad = genCode(2);         % Spectral radius of the reservoir
config.noise_level = genCode(3);             % Variance of noise during state update
config.lambda = 2^genCode(4);                % Regularization parameter
config.input_scaling = genCode(5);           % Input scaling
config.feedback_scaling = genCode(6);        % Feedback scaling (set to 0 for no feedback)
config.teacher_scaling = genCode(7);         % Teacher scaling


% ESN 
curr_seed = rng;
rng(config.rseed);
[ ~, fitness, ~, ~, ~ ] = esn_prediction( {obsData_tr}, {obsData_ts}, {futureData_tr}, {futureData_ts}, config );

rng(curr_seed);

end

