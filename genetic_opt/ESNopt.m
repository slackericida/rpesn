function [ configSet ] = ESNopt( obsData_tr, futureData_tr, obsData_ts, futureData_ts, configSet)


if(configSet{1}.esn_opt)
    
    fprintf('ESN genetic optimization...');
    
    % GA parameters
    pop_size=10;
    Generation_Number=2;
    code_length=7;
    LB = [10,      0.5,     0.00001,     -10,      0.1,       0,    0.1 ];
    UB = [100,    0.99,     0.01,         10,      0.6,     0.6,    0.6 ];
    IntCon = [1,4];
    
    
    if isempty(gcp('nocreate'))
        parpool;
    end
    
    for k=1:configSet{1}.K   
               
        % GA optimization
        opts = gaoptimset('PopulationSize',pop_size,'PlotFcns',@gaplotbestf,'Generation',Generation_Number,'UseParallel', 'always','vectorized','off');
        [genCode] = ga(@(p)esn_fitfun(p, obsData_tr(:,k), obsData_ts(:,k), futureData_tr(:,k), futureData_ts(:,k), configSet{k}, training_signal), code_length,[],[],[],[],LB,UB,[],IntCon,opts);
        
        % Read optimal gen code
        configSet{k}.reservoir_size = 10*genCode(1);                         % Size of the reservoir
        configSet{k}.desired_specrad = genCode(2);                           % Spectral radius of the reservoir
        configSet{k}.noise_level = genCode(3);                               % Variance of noise during state update
        configSet{k}.lambda = 2^genCode(4);                                  % Regularization parameter (lin_reg, lasso, SVR)
        configSet{k}.input_scaling = genCode(5);                             % Input scaling
        configSet{k}.feedback_scaling = genCode(6);                          % Feedback scaling (set to 0 for no feedback)
        configSet{k}.teacher_scaling = genCode(7);                           % Teacher scaling
                
    end
    
    fprintf('Done\n');
    
else
    fprintf('ESN without optimization...\n');
    
    for k=1:configSet{1}.K

        configSet{k}.reservoir_size = 100;                                   % Size of the reservoir
        configSet{k}.desired_specrad = 0.9;                                  % Spectral radius of the reservoir
        configSet{k}.noise_level = 0;                                   % Variance of noise during state update
        configSet{k}.lambda = 0.1;                                           % Regularization parameter
        configSet{k}.input_scaling = 0.1;                                    % Input scaling
        configSet{k}.feedback_scaling = 0;                                 % Feedback scaling (set to 0 for no feedback)
        configSet{k}.teacher_scaling = 0.1;                                  % Teacher scaling
    end
end


end

