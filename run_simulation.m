% RUN_SIMULATION - Main simulation file
% Change values in `config.m' to modify the parameters of the run

close all; 
%clear all;

% Adds the folders to the path
new_path = genpath(fileparts(pwd));
addpath(new_path);

RR=[]; DET=[]; L=[]; Lmax=[]; ENTR=[]; LAM=[]; TT=[]; Vmax=[]; t1=[]; t2=[]; RTE=[]; Clust=[]; Trans=[];
Mlist = cell(0);

for j=1:10

    config_simulation;
    RRi=[]; DETi=[]; Li=[]; Lmaxi=[]; ENTRi=[]; LAMi=[]; TTi=[]; Vmaxi=[]; t1i=[]; t2i=[]; %RTE=[]; Clust=[]; Trans=[];
    Mlist = cell(0);

    for i=0.05:0.05:2
        specrad = i;

        % initialize variables
        test_series = input_series(end-test_elements+1:end);
        config = configSet{1};
        config.desired_specrad = i;

        % run ESN
        rng(config.rseed)
        [ esn_forecast, ~, esn_time, ~, stateMatrix ] = esn_prediction( {obsData_tr}, {obsData_ts}, {futureData_tr}, {futureData_ts}, config );
        esn_error = config.err_fcn(esn_forecast, test_series);

%         plot_periods = test_elements;
% 
%         % Plot prediction results
%         legendNames = cell(0);
%         legendNames{1} = 'Observed';
%         legendNames{2} = 'ESN';
%         plot_outputs('Prediction Results', legendNames, test_series(1:plot_periods), [esn_forecast(1:plot_periods)]);
%         figshift;
% 
%         % RP
%         figure;
%         plot(stateMatrix);

        stateSeq = stateMatrix(:,1:config.reservoir_size);
        M = zeros(test_elements);

        for i = 1:test_elements
            Si = stateSeq(i,:);

            for j=i+1:test_elements
                Sj = stateSeq(j,:);
                d_ij = sum(abs(Si - Sj));
                M(i,j) = d_ij;
            end

        end

        M = flipud(M + M');
        Mlist = [Mlist; M];

        M_mean = mean(mean(M))*0.7;
        MD = nan(size(M));
        MD(M < M_mean) = 1;
        MD(M > M_mean) = 0;
%         imagesc(1-MD)
%         colormap(gray)

        global GRM;
        GRM=MD;
        RQA_measures=crqa_custom(randn(300,1));

        RRi = [RRi; RQA_measures(1)]; DETi=[DETi; RQA_measures(2)]; Li=[Li; RQA_measures(3)]; Lmaxi=[Lmaxi; RQA_measures(4)]; ENTRi=[ENTRi; RQA_measures(5)]; 
        LAMi=[LAMi; RQA_measures(6)]; TTi=[TTi; RQA_measures(7)]; Vmaxi=[Vmaxi; RQA_measures(8)]; t1i=[t1i; RQA_measures(9)]; t2i=[t2i; RQA_measures(10)];

    end
    
    RR = [RR,RRi]; DET=[DET,DETi]; L=[L,Li]; Lmax=[Lmax,Lmaxi]; ENTR=[ENTR,ENTRi]; LAM=[LAM,LAMi]; TT=[TT,TTi]; 
    Vmax=[Vmax, Vmaxi]; t1=[t1, t1i]; t2=[t2, t2i];
    
end

ENTRm = mean(ENTR,2); ENTRstd = std(ENTR,0,2);
DETm = mean(DET,2); DETstd = std(DET,0,2);
RRm = mean(RR,2); RRstd = std(RR,0,2);
Lm = mean(L,2); Lstd = std(L,0,2);
Lmaxm = mean(Lmax,2); Lmaxstd = std(Lmax,0,2);
LAMm = mean(LAM,2); LAMstd = std(LAM,0,2);
TTm = mean(TT,2); TTstd = std(TT,0,2);
Vmaxm = mean(Vmax,2); Vmaxstd = std(Vmax,0,2);
t1m = mean(t1,2); t1std = std(t1,0,2);
t2m = mean(t2,2); t2std = std(t2,0,2);


figure
subplot(3,3,1)
plot(DETm, 'k')
hold on
plot(DETm+DETstd, 'r--')
plot(DETm-DETstd, 'r--')
hold off
title('DET')

subplot(3,3,2)
plot(ENTRm, 'k')
hold on
plot(ENTRm+ENTRstd, 'r--')
plot(ENTRm-ENTRstd, 'r--')
hold off
title('ENTR')

subplot(3,3,3)
plot(Lm, 'k')
hold on
plot(Lm+Lstd, 'r--')
plot(Lm-Lstd, 'r--')
hold off
title('L')

subplot(3,3,4)
plot(LAMm, 'k')
hold on
plot(LAMm+LAMstd, 'r--')
plot(LAMm-LAMstd, 'r--')
hold off
title('LAM')

subplot(3,3,5)
plot(Lmaxm, 'k')
hold on
plot(Lmaxm+Lmaxstd, 'r--')
plot(Lmaxm-Lmaxstd, 'r--')
hold off
title('Lmax')

subplot(3,3,6)
plot(RRm, 'k')
hold on
plot(RRm+RRstd, 'r--')
plot(RRm-RRstd, 'r--')
hold off
title('RR')

subplot(3,3,7)
plot(ENTRm, 'k')
hold on
plot(ENTRm+ENTRstd, 'r--')
plot(ENTRm-ENTRstd, 'r--')
hold off
title('t2')

subplot(3,3,8)
plot(TTm, 'k')
hold on
plot(TTm+TTstd, 'r--')
plot(TTm-TTstd, 'r--')
hold off
title('TT')

subplot(3,3,9)
plot(Vmaxm, 'k')
hold on
plot(Vmaxm+Vmaxstd, 'r--')
plot(Vmaxm-Vmaxstd, 'r--')
hold off
title('Vmax');

% % Save the main results of the simulation
% formatOut = 'mmdd-HHMM';
% save([datestr(now,formatOut),'.mat'],'configSet','est_noise','esn_error','series','noise','signal')
save('RQA_measures.mat','RR', 'DET', 'L', 'Lmax', 'ENTR', 'LAM', 'TT', 'Vmax', 't1', 't2', 'RRm', 'DETm', 'Lm', 'Lmaxm', 'ENTRm', 'LAMm', 'TTm', 'Vmaxm', 't1m', 't2m', 'RRstd', 'DETstd', 'Lstd', 'Lmaxstd', 'ENTRstd', 'LAMstd', 'TTstd', 'Vmaxstd', 't1std', 't2std')

rmpath(new_path);
