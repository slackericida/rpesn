function [obsData, futureData] = construct_output( X, tau )
% CONSTRUCT_OUTPUT Construct the matrix of the observed data and the matrix of the future data,
% shifting the values in X by tau.
%
% INPUT: 
% X - input Time Series
% tau - step of the prediction
%
% OUTPUT: 
% obsData: current values of the TS
% futureData: future values of the TS, shifted by tau

% Construct the output vector, which is the input shifted by tau
futureData = X(tau+1:end, :);

% Delete the last elements from the input matrix
obsData = X(1:end-tau, :);


end

