function [ X ] = dataNorm( X, norm_type )
%DATANORM Normalize the data according to one of the following procedures
%
%       1 - rescaling in [-1,1]
%       2 - rescaling in [0, 1]
%       3 - standardization
%       4 - tanh of (data - mean )
%       5 - sqrt of (data + 1/4) 
%       6 - log of data 

if(norm_type == 1)                                                          % rescaling in [-1, 1]
    X = mapminmax(X', -1, +1)';
elseif(norm_type == 2)                                                      % rescaling in [0, 1]
    X = mapminmax(X', 0, +1)';
elseif(norm_type == 3)                                                      % standardization
    X = zscore(X);
elseif(norm_type == 4)                                                      % tanh normX
    X_mean = mean(X);
    N_samples = size(X,1);
    X = tanh(X - repmat(X_mean, [N_samples,1]));
elseif(norm_type == 5)                                                      % sqrt(x +1/4)
    X = sqrt(X + 1/4)+0.1;
elseif(norm_type == 6)                                                      % log(x)
    X = log(X+0.1);
end


end

