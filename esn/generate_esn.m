function esn = generate_esn(nInputUnits, nInternalUnits, nOutputUnits, varargin)
% GENERATE_ESN - Generate an ESN struct
% Inputs:
%   - nInputUnits: number of input units.
%   - nInternalUnits: sizeo of the reservoir.
%   - nOutputUnits: number of output units.
%   - varargin: additional parameters in the form name/value. They are:
%       * inputScaling: scaling of the input matrix, default 1.
%       * inputShift: shift of the input signal, default 0.
%       * teacherScaling: scaling of the teacher signal, default 1.
%       * teacherShift: shift of the teacher signal, default 0.
%       * feedbackScaling: scaling of the feedback matrix, default 0.
%       * noiseLevel: noise level during the state update, default 0.
%       * spectralRadius: initial spectral radius, default 0.9.
%       * connectivity: percentage of non-zero connections in the
%       reservoir, default 0.
% Outputs:
%   - esn: a struct with all the required matrices and parameters.

esn.nInternalUnits = nInternalUnits; 
esn.nInputUnits = nInputUnits; 
esn.nOutputUnits = nOutputUnits; 
  
% Parse the optional parameters
p = inputParser();
p.addParamValue('inputScaling', ones(nInputUnits, 1), @(x) assert(length(x) == esn.nInputUnits, 'The size of the inputScaling does not match the number of input units'));
p.addParamValue('inputShift', zeros(nInputUnits, 1), @(x) assert(length(x) == esn.nInputUnits, 'The size of the inputShift does not match the number of input units'));
p.addParamValue('teacherScaling', ones(nOutputUnits, 1), @(x) assert(length(x) == esn.nOutputUnits, 'The size of the teacherScaling does not match the number of output units'));
p.addParamValue('teacherShift', zeros(nOutputUnits, 1), @(x) assert(length(x) == esn.nOutputUnits, 'The size of the teacherShift does not match the number of output units'));
p.addParamValue('feedbackScaling', zeros(nOutputUnits, 1), @(x) assert(length(x) == esn.nOutputUnits, 'The size of the feedbackScaling does not match the number of output units'));
p.addParamValue('noiseLevel', 0.0, @(x) assert(isscalar(x) && x >= 0, 'The noiseLevel must be a non-negative scalar'));
p.addParamValue('spectralRadius', 0.9, @(x) assert(isscalar(x) && x >= 0, 'The spectralRadius must be a non-negative scalar'));
p.addParamValue('connectivity', 1, @(x) assert(isscalar(x) && x >= 0, 'The connectivity must be a scalar in [0, 1]'));

p.parse(varargin{:});
esn.inputScaling = p.Results.inputScaling;
esn.inputShift = p.Results.inputShift;
esn.teacherScaling = p.Results.teacherScaling;
esn.teacherShift = p.Results.teacherShift;
esn.feedbackScaling = p.Results.feedbackScaling;
esn.noiseLevel = p.Results.noiseLevel;
esn.spectralRadius = p.Results.spectralRadius;
nTotalUnits = nInternalUnits + nInputUnits + nOutputUnits; 
esn.nTotalUnits = nTotalUnits; 

% Compute the internal matrix
esn.internalWeights_UnitSR = generate_internal_weights(nInternalUnits, p.Results.connectivity);
esn.internalWeights = esn.spectralRadius * esn.internalWeights_UnitSR; % Set the spectral radius

% Compute input and feedback weight matrices
esn.inputWeights = 2.0 * rand(nInternalUnits, nInputUnits)- 1.0;
esn.feedbackWeights = (2.0 * rand(nInternalUnits, nOutputUnits)- 1.0);   




