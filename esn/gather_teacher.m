function teacherCollection = gather_teacher( outputSequences, esn, nForgetPoints, shift )
% GATHER_TEACHER - Compute the teacher signal
%   This concatenates the (suitably scaled and shifted) teacher signal from
%   multiple output sequences, by removing the dropout elements.
% Inputs:
%   - outputSequences: output sequences.
%   - esn: ESN struct created with generate_esn.m.
%   - nForgetPoints: number of dropout elements.
% Outputs:
%   - teacherCollection: teacher matrix.

sampleSize = 0;
nTimeSeries = size(outputSequences, 1);
for i = 1:nTimeSeries
    sampleSize = sampleSize + size(outputSequences{i,1},1) - max([0, nForgetPoints]);
end

teacherCollection = zeros(sampleSize, esn.nOutputUnits);

collectIndex = 1;
for i = 1:nTimeSeries
    teacherCollection_i = compute_teacher(outputSequences{i,1}, esn, nForgetPoints, shift);
    l = size(teacherCollection_i, 1);
    teacherCollection(collectIndex:collectIndex+l-1, :) = teacherCollection_i;
    collectIndex = collectIndex + l;
end

end

