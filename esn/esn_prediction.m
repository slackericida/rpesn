function [ predictedTestOutput, error, time, teacherCollection, stateMatrix ] = esn_prediction( X_train, X_test, Y_train, Y_test, config )
%ESN_PREDICTION Compute the prediction with ESN. The ESN is trained using the values in config

% Initialize Variables
error = 0;
time = 0;
inputSize = size(X_train{1});
outputSize = size(Y_train{1});

for r_ii = 1:config.n_runs
        
    fprintf('--- RUN %i / %i ---\n', r_ii, config.n_runs);

    % Initialize the ESN structure
    esn_o = generate_esn(inputSize(2), config.reservoir_size, outputSize(2), ...
        'spectralRadius', config.desired_specrad, 'inputScaling', repmat(config.input_scaling, inputSize(2), 1) ,'inputShift', repmat(config.input_shift, inputSize(2), 1), ...
        'teacherScaling', repmat(config.teacher_scaling, outputSize(2), 1), 'teacherShift', repmat(config.teacher_shift, outputSize(2), 1), 'feedbackScaling', repmat(config.feedback_scaling, outputSize(2), 1), ...
        'noiseLevel', config.noise_level, 'connectivity', config.connectivity);
    
    % Print information on screen
    fprintf('Size: %i training samples, %i test samples\n', size(X_train{1}, 1), size(X_test{1}, 1));
    
    % Train ESN
    fprintf('[ESN] Training...\n');
    [error_c, trainInfo] = train_esn(X_train, Y_train, X_test, Y_test, esn_o, config);
    error = error + error_c;
    time = time + trainInfo.gathering_time;
        
end

% Run the ESN a final time (to plot output and states)
[~, predictedTestOutput, teacherCollection, stateMatrix] = test_esn(X_test, Y_test, trainInfo.esn, config);


end

