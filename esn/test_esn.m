function [testError, esn_output, teacher_output, stateCollection] = test_esn(inputSequence, outputSequence, esn, config)
% TEST_ESN - Test a trained ESN
% Inputs:
%   - inputSequence: test input sequences.
%   - outputSequence: test output sequences.
%   - esn: ESN struct.
%   - config: configuration struct created in config.m.

% Compute the states
stateCollection = gather_states( inputSequence, [], esn, config.dropout, config );

% Compute the predicted output
if(strcmp(config.rTrain, 'lin_svr') || strcmp(config.rTrain, 'rbf_svr'))
    [~, ~, esn_output] = svmpredict(zeros(size(stateCollection, 1),1), stateCollection, esn.svrModel, '-q 1'); % che � sto q?
else % lin_reg and lasso
    esn_output = stateCollection * esn.outputWeights' ;
end

% shift and scale the predicted output
nOutputPoints = length(esn_output(:,1)) ;
esn_output = esn_output - repmat(esn.teacherShift',[nOutputPoints 1]) ;
esn_output = esn_output / diag(esn.teacherScaling) ;

% Gather the teacher signal
teacher_output = gather_teacher( outputSequence, esn, config.dropout, false );

% Compute the error
testError = config.err_fcn(esn_output, teacher_output);