function stateCollectMat = compute_statematrix(inputSequence, outputSequence, esn, nForgetPoints, config)
% INPUT:
% - inputSequence: N x M matrix of inputs (N = number of observations, M = dimension of the input)
% - outputSequence: n x m matrix of outputs (n = number of observations, m = dimension of the input)
% - esn: echo state network to be trained
% - nForgetPoints: number of the first states that must be ignored 
%
% OUTPUT:
% - stateCollectMat: collection of internal states of the system
%
% Copyright: Fraunhofer IAIS 2006 / Patent pending

if isempty(inputSequence) && isempty(outputSequence)
    error('error in compute_statematrix: two empty input args');
end

nDataPoints = length(inputSequence(:,1));

% compute the states in teaching mode (training) or not (test)
if isempty(outputSequence)
    teacherForcing = 0;    
else
    teacherForcing = 1;
end

% number of states to be ignored (at the beginning the network is evolving toward its stable configuration)
if nForgetPoints >= 0
    stateCollectMat = zeros(nDataPoints - nForgetPoints, esn.nInputUnits + esn.nInternalUnits) ; 
else
    stateCollectMat = zeros(nDataPoints,                 esn.nInputUnits + esn.nInternalUnits) ; 
end

%% set starting state
totalstate = zeros(esn.nInternalUnits + esn.nInputUnits + esn.nOutputUnits, 1);

for i = 1:nDataPoints
    
    % scale and shift the value of the inputSequence
    in = esn.inputScaling .* inputSequence(i,:)' + esn.inputShift;  % in is column vector

    % write input into totalstate
    totalstate(esn.nInternalUnits+1:esn.nInternalUnits + esn.nInputUnits) = in;
    
    % compute the new state using the previois state, the new input and the previous output
    internalState =  tanh([esn.internalWeights , esn.inputWeights , esn.feedbackWeights * diag(esn.feedbackScaling)] * totalstate)  ;   
    internalState = internalState + esn.noiseLevel * (rand(esn.nInternalUnits,1) - 0.5) ; % add noise

    % compute the output of the network using a shifted value of the real output (teaching mode) or the input+state through the output
    if teacherForcing
        netOut = esn.teacherScaling .* outputSequence(i,:)' + esn.teacherShift;
    else        
        if(strcmp(config.rTrain, 'lin_svr') || strcmp(config.rTrain, 'rbf_svr'))
            [~, ~, netOut] = svmpredict(0, [internalState; in]', esn.svrModel, '-q 1');
        else % lin_reg and lasso
            netOut = esn.outputWeights * [internalState; in];
        end    
    end
    
    totalstate = [internalState; in; netOut];
    
    % add the last internal states and inputs to the results
    if  i > nForgetPoints
        stateCollectMat(i-nForgetPoints,:) = [internalState', in']; 
    end
    
end
