% CONFIG_SIMULATION - Define the configuration for the current run

% -------------------------------------------------------------------------
% --- Dataset -------------------------------------------------------------
% -------------------------------------------------------------------------  
input_series = sin(linspace(1,100,10000)*3)';
% input_series = logistic_map(0.5,3.999,10000);
% input_series = randn(10000,1);
% t = linspace(0, 0.2, 200)';
% input_series = cos(2*pi*1000*t + 0.5*sin(2*pi*25*t));

config.forecastStep = 15; % how far the prediction is made
config.dropout = 100; % Number of dropout elements of the ESN
test_elements = 1500; % number of test elements
config.K = 1; 

% construct input and output
[obsData, futureData] = construct_output(input_series, config.forecastStep);

% Construct Training and Test set
tr_size = (floor(size(obsData,1)) - config.dropout - test_elements);
obsData_tr = obsData(1:tr_size, :);
futureData_tr = futureData(1:tr_size, :);
obsData_ts = obsData(tr_size+1:end, :);
futureData_ts = futureData(tr_size+1:end, :);
   

% -------------------------------------------------------------------------
% --- Error measure -------------------------------------------------------
% -------------------------------------------------------------------------
%config.err_fcn = @mre;     % Mean Relative Error
config.err_fcn = @nrmse;    % Normalized Root Mean-Squared Error

% -------------------------------------------------------------------------
% --- ESN parameters ------------------------------------------------------
% -------------------------------------------------------------------------
config.esn_opt = 0;
config.rseed = rng;                                                % seed used for generating the network

% Fixed params
config.n_runs = 1;                                                 % Repetitions of the simulation
config.input_shift = 0;                                            % Input shift
config.teacher_shift = 0;                                          % Teacher shift
config.connectivity = 0.25;                                        % Percentage of non-zero connections in the reservoir                                          
config.rTrain = 'lin_reg';                                         % Readout Training

% optimization esn
configSet{1} = config;
configSet = ESNopt(obsData_tr, futureData_tr, obsData_ts, futureData_ts, configSet);
