% NSR - Compute the noise-to-signal ratio

function NSR = nsr(estimatedOutput, correctOutput)

NSR = 10*log10(sum((correctOutput-estimatedOutput).^2)/sum(correctOutput.^2));
