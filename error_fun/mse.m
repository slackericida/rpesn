% MSE - Compute the mean-squared error

function MSE = mse(estimatedOutput, correctOutput)
  
nEstimatePoints = size(estimatedOutput, 1) ; 

nForgetPoints = size(correctOutput, 1) - nEstimatePoints ;

correctOutput = correctOutput(nForgetPoints+1:end,:) ; 
 
MSE = sum((estimatedOutput - correctOutput).^2)/nEstimatePoints ; 
