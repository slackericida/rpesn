function [ Y ] = logistic_map( y0, r, n )
%LOGISTIC_MAP 
% y0 = initial value
% r = degree
% n = number of generation (length of output vector Y)

Y = nan(n,1);
Y(1) = y0;

for i=1:n
    Y(i+1) = r*Y(i)*(1-Y(i));
end

plot(Y)

end

