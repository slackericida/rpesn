function [inputSequences, outputSequences] = ext_poly(sequenceLength, n_sequences, p, d)
% EXT_POLY - Extended polynomial as described in:
%   [1] Butcher, J. B., Verstraeten, D., Schrauwen, B., Day, C. R., & Haycock, 
%   P. W. (2013). Reservoir computing and extreme learning machines for 
%   non-linear time-series data analysis. Neural networks, 38, 76-89.
% Inputs:
%   - sequenceLenght: length of each sequence
%   - n_sequences: number of sequences to be generated
%   - p, d: power of the polynomial terms, and delay.
% Outputs:
%   - inputSequences: cell array of input sequences
%   - outputSequences: cell array of output sequences

inputSequences = cell(n_sequences, 1);
outputSequences = cell(n_sequences, 1);

% Extract the parameters
C = rand(p + 1, p + 1)*2 - 1;
for ii = 2:p+1
    C(ii, end-ii+2:end) = 0;
end

exponents = (0:p)';

for n = 1:n_sequences

    inputSequences{n} = [ones(sequenceLength,1) (rand(sequenceLength,1)*2 - 1)];
    outputSequences{n} = zeros(sequenceLength, 1);
    
    for i = d + 1 : sequenceLength
        u1 = repmat(inputSequences{n}(i, 2), p+1, 1).^exponents;
        u2 = repmat(inputSequences{n}(i - d, 2), p+1, 1).^exponents;
        outputSequences{n}(i) = sum(sum(C.*(u1*u2')));
    end
    
    outputSequences{n} = tanh(outputSequences{n} - mean(outputSequences{n}));
    
end